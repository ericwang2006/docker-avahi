# Avahi Docker Image

## Configuration

Put your service definition files in a directory and mount it as a volume to _/etc/avahi/services_.

### Options

Configuration is done through environment variables.

- **ALLOW_INTERFACES** set _allow-interfaces_ in avahi-daemon.conf
- **DENY_INTERFACES** set _deny-interfaces_ in avahi-daemon.conf

## Start the container

```bash
docker run -d --restart always \
  --net=host \
  -e ALLOW_INTERFACES=eth0 \
  -v $(pwd)/services:/etc/avahi/services \
  ydkn/avahi:latest
```
